import unittest
from .api_base_matrix import BaseTestMatrix
from smp.matrix import Matrix
from smp.matrix_parallel_process import MatrixParallelProcess
from smp.matrix_parallel_thread import MatrixParallelThread
from mpp.matrix import MatrixCluster

"""Тест реализации простого умножения матриц с одним потоком исполнения"""


class TestMatrix(unittest.TestCase, BaseTestMatrix):
    def setUp(self):
        self.init(Matrix)
        self.mul_methods = Matrix.get_mul_methods()


"""Тест параллельного умножения матриц"""

class TestMatrixParallelProcess(unittest.TestCase, BaseTestMatrix):
    def setUp(self):
        self.init(MatrixParallelProcess)
        self.mul_methods = MatrixParallelProcess.get_mul_methods()


class TestMatrixParallelThread(unittest.TestCase, BaseTestMatrix):
    def setUp(self):
        self.init(MatrixParallelThread)
        self.mul_methods = MatrixParallelThread.get_mul_methods()


class TestMatrixCluster(unittest.TestCase, BaseTestMatrix):
    def setUp(self):
        self.init(MatrixCluster)
        self.mul_methods = MatrixCluster.get_mul_methods()
