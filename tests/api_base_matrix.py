import unittest


class BaseTestMatrix(object):
    def __init__(self):
        pass

    def init(self, class_obj):
        self.class_obj = class_obj

        self.mul_methods = []

    def test_eq_method(self):
        """Проверка реализации сравнения матриц"""
        self.assertEqual(self.class_obj(4, 4), self.class_obj(4, 4))
        self.assertNotEqual(self.class_obj(4, 3), self.class_obj(4, 4))
        self.assertNotEqual(self.class_obj(4, 4, [i for i in range(4 * 4)]), self.class_obj(4, 4))

    def test_mul_matrix3x3(self):
        """Проверка умножения матриц малой размерности, c предварительно рассчитаным результатом"""
        a = [3, 2, 9,
             8, 1, 6,
             7, 4, 0]
        lhs = self.class_obj(3, 3, a)

        b = [7, 6, 8,
             4, 5, 9,
             2, 5, 2]
        rhs = self.class_obj(3, 3, b)

        c = [47, 73, 60,
             72, 83, 85,
             65, 62, 92]
        result = self.class_obj(3, 3, c)

        for i in self.mul_methods:
            with self.subTest(i=i):
                self.assertEqual(result, i(lhs, rhs))

    def test_mul_matrix3x2(self):
        """Проверка умножения матриц малой размерности, c предварительно рассчитаным результатом"""
        a = [64, 0, 16,
             57, 84, 5,
             14, 5, 15]
        lhs = self.class_obj(3, 3, a)

        b = [53, 5,
             2, 9,
             3, 7]
        rhs = self.class_obj(3, 2, b)

        c = [3440, 432,
             3204, 1076,
             797, 220]
        result = self.class_obj(3, 2, c)

        for i in self.mul_methods:
            with self.subTest(i=i):
                self.assertEqual(result, i(lhs, rhs))
