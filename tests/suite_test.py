# -*- coding: utf-8 -*-
import unittest
from tests.unit_matrix_classes import TestMatrix, TestMatrixParallelProcess, TestMatrixParallelThread, TestMatrixCluster

if __name__ == "__main__":
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(TestMatrix))
    suite.addTest(unittest.makeSuite(TestMatrixParallelThread))
    suite.addTest(unittest.makeSuite(TestMatrixParallelProcess))
    suite.addTest(unittest.makeSuite(TestMatrixCluster))
    unittest.main()
