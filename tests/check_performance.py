import random
import time
from struct_matrix import StructMatrix
from smp.matrix import Matrix
from smp.matrix_parallel_process import MatrixParallelProcess
from smp.matrix_parallel_thread import MatrixParallelThread

# cu - compute unit 0 - auto, othe count cu

def randomMatrix():
    return random.randint(-100, 100)

def performance_test(f, arg, warg):
    pass

def performance(class_obj, methods, size, count_test,  cu):
    k = size
    A = class_obj(k, k, randomMatrix)
    B = class_obj(k, k, randomMatrix)
    #standard = A * B  # эталон решения
    StructMatrix.setComputeUnit(cu)
    result = []
    for method in methods:
        log = []
        for i in range(count_test):
            start = time.time()
            ret = method(A, B)
            end = time.time()
            delta = end - start
            log.append(delta)
        print("Метод %s Время:%6.4f" % (method.__name__, delta))
        result.append({method.__name__: log})
    return result


def test(size, rans, cu=None):
    # test_classes
    test_classes = [
        (Matrix, Matrix.get_mul_methods()),
        (MatrixParallelProcess, MatrixParallelProcess.get_mul_methods()),
        (MatrixParallelThread, MatrixParallelThread.get_mul_methods())
    ]
    print("Тестирование производительности кода:")
    result = []
    for class_obj, methods in test_classes:
        print("Тест класса:%s" % class_obj.__name__)
        result.append({
            class_obj.__name__: performance(class_obj, methods, size, rans, cu)
        })
    return result
