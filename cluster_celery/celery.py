from celery import Celery
import logging
from celery.utils.log import get_task_logger
import cluster_celery.celeryconfig as config

app = Celery('cluster_celery',
             backend='amqp',
             broker='amqp://gust_celery:pass@192.168.0.106/my_host2',
            include=['cluster_celery.tasks'])

app.config_from_object(config)
# run worker
# <dir_project>./celery -A cluster_celery.tasks worker --loglevel=info

# logger = get_task_logger(__name__)

# logging.basicConfig(level=logging.DEBUG,
#                     format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
#                     datefmt='%m-%d %H:%M',
#                     filename='./myapp.log',
#                     filemode='w')
# CELERY_ACCEPT_CONTENT = ['json', 'pickle']
# CELERY_TASK_RESULT_EXPIRES=3600
# #CELERY_RESULT_BACKEND='amqp'
# CELERY_RESULT_BACKEND='djcelery.backends.database:DatabaseBackend'
# # postgresql
# #CELERY_RESULT_BACKEND = 'db+postgresql://postgres:111222@localhost/graphgrail'
# #CELERY_RESULT_BACKEND='djcelery.backends.cache:CacheBackend',
# CELERY_RESULT_SERIALIZER = 'json' #json pickle msgpack
# CELERY_TASK_SERIALIZER = 'json'
