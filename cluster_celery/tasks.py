from cluster_celery.celery import app
import time
import numpy as np

@app.task(ignore_result = True)
def sleep_task():
    time.sleep(10)

@app.task(acks_late = True)
def mulc(pos, vec_a, vec_b):
    """Умножение матриц с кешированием столбца и с использованием скалярного умножения векторов numpy"""
    i, j = pos
    return (i, j, np.dot(vec_a, vec_b))

if __name__ == '__main__':
    app.worker_main()