from array import *
from celery import group
from cluster_celery.tasks import mulc
from struct_matrix import StructMatrix

# TODO добавить умное определени типа массива результата

class MatrixCluster(StructMatrix):

    def mul1c(self, other):
        """Умножение матриц с кешированием столбца и с использованием скалярного умножения векторов numpy"""
        vector_len = self._M
        ret = self.make_result(other)
        bath = []
        for j in range(other._M):  # обход по-столбцам
            col = array("d", (other._array[(k * other._M) + j] for k in range(vector_len)))
            # for i in range(self._N):  # обход по-строчно
            for i in range(self._M):
                bath.append(mulc.s((i, j),
                                   self._array[i * vector_len:i * vector_len + vector_len],
                                   col[0:vector_len]))
        result = group(bath)
        t = result()
        g = t.get()
        for r in g:
            i, j, value = r
            ret._array[i * other._M + j] = value
        return ret

    def mul2c(self, other):
        """Умножение матриц с кешированием столбца и с использованием скалярного умножения векторов numpy"""
        vector_len = self._M
        ret = self.make_result(other)
        bath = []
        for j in range(other._M):  # обход по-столбцам
            col = array("d", (other._array[(k * other._M) + j] for k in range(vector_len)))
            result = group(mulc.s((i, j),
                                       self._array[i * vector_len:i * vector_len + vector_len],
                                       col[0:vector_len]) for i in range(self._M))
            t = result()
            g = t.get()
            for r in g:
                i, j, value = r
                ret._array[i * other._M + j] = value
        return ret

    @classmethod
    def get_mul_methods(cls):
        return [MatrixCluster.mul2c]

    def __init__(self, n, m, initializer=None, type_values='d'):
        StructMatrix._array_type = array
        StructMatrix.__init__(self, n, m, initializer, type_values)

    def __mul__(self, other):
        return MatrixCluster.get_mul_methods()[-1](self, other)
