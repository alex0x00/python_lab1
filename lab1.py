# -*- coding: utf-8 -*-
from matrix_parallel_process import MatrixParallelProcess
from matrix_parallel_thread import MatrixParallelThread
from matrix import Matrix
from test_matrix import TestMatrix
from test_matrix_parallel import TestMatrixParallelProcess, TestMatrixParallelThread
import unittest
import test_performance
import json

if __name__ == "__main__":
    result = []
    samples = 1 # количество замеров производительности отдельного метода
    check_on_cu = [1, 2, 4, 8] # количество используемых вычислительных модулей
    for i in range(1, 4):
        for cu in check_on_cu:
            result.append({
                "cu": cu,
                "size": 10**i,
                "statistic": test_performance.test(10**i, samples, cu)
            })
    print(result)
# import json
# with open('result.json', 'w') as outfile:
#     json.dump(result, outfile,
#               ensure_ascii=False,
#               sort_keys=True,
#               indent=4)
    a = [64, 0, 16,
         57, 84, 5,
         14, 5, 15]
    lhs = Matrix(3, 3, a)

    b = [53, 5,
         2, 9,
         3, 7]
    rhs = Matrix(3, 2, b)

    c = [3440, 432,
         3204, 1076,
         797, 220]
    # d = MatrixParallelThread(3, 3)
    # l = MatrixParallelThread(3, 2)
    g = lhs * rhs #смешивание типов матриц
    print(g)
    #
    # suite = unittest.TestSuite()
    # suite.addTest(unittest.makeSuite(TestMatrix))
    # # suite.addTest(unittest.makeSuite(TestMatrixParallelThread))
    # # suite.addTest(unittest.makeSuite(TestMatrixParallelProcess))
    # unittest.main()


# if __name__ == '__main__':
#     multiprocessing.freeze_support()
#     main()




