import multiprocessing as multiprocessing
from multiprocessing.sharedctypes import RawArray

from smp.multiplication_functions import mul1p, mul2p, mul3p
from struct_matrix import StructMatrix


# TODO добавить умное определени типа массива резльтата

class MatrixParallelProcess(StructMatrix):

    @classmethod
    def get_mul_methods(cls):
        return [mul1p, mul2p, mul3p]

    def __init__(self, n, m, initializer=None, type_values='d'):
        StructMatrix._array_type = RawArray
        StructMatrix.__init__(self, n, m, initializer, type_values)

    def getMultiprocessing(self):
        return multiprocessing

    def __mul__(self, other):
        return MatrixParallelProcess.get_mul_methods()[-1](self, other)

