from smp.workers_mul import *

def mul1(self, other):
    """Простое умножение матриц"""
    vector_len = self._M
    ret = self.make_result(other)
    for i in range(self._N):  # обход по-строчно
        for j in range(other._M):  # обход по-столбцам
            sum = 0
            for k in range(vector_len):
                sum += self.get_item(i, k) * other.get_item(k, j)
            ret.set_item(i, j, sum)
    return ret


def mul2(self, other):
    """Умножение матриц с кешированием столбца"""
    vector_len = self._M
    ret = self.make_result(other)
    col = array("d", (0 for i in range(vector_len)))
    for j in range(other._M):  # обход по-столбцам
        # копируем столбец правой матрицы
        for k in range(vector_len): col[k] = other.get_item(k, j)
        for i in range(self._N):  # обход по-строчно
            sum = 0
            for k in range(vector_len): sum += self.get_item(i, k) * col[k]
            ret.set_item(i, j, sum)
    return ret


def mul3(self, other):
    """Умножение матриц с кешированием столбца и оптимизации индексирования"""
    vector_len = self._M
    ret = self.make_result(other)
    for j in range(other._M):  # обход по-столбцам
        col = array("d", (other._array[(k * other._M) + j] for k in range(vector_len)))
        for i in range(self._N):  # обход по-строчно
            sum = 0
            p = i * self._M
            for k in range(vector_len):
                sum += self._array[p + k] * col[k]
            ret._array[i * ret._M + j] = sum
    return ret


def mul4(self, other):
    """Умножение матриц с кешированием столбца и с использованием скалярного умножения векторов numpy"""
    vector_len = self._M
    ret = self.make_result(other)
    for j in range(other._M):  # обход по-столбцам
        col = array("d", (other._array[(k * other._M) + j] for k in range(vector_len)))
        for i in range(self._N):  # обход по-строчно
            p = i * vector_len
            sum = np.dot(self._array[p:p + vector_len], col[0:vector_len])
            ret._array[i * other._M + j] = sum
    return ret


def mul1p(self, other):
    """Обычное параллельное умножение матриц"""
    indexes_vectors = ((i, self._M, other._M) for i in range(self._N))
    return mulParallelEnv(self, other, worker_mul1, indexes_vectors)


def mul2p(self, other):
    """Параллельное умножение матриц с кешированием строки"""
    indexes_vectors = ((j, self._M, self._N, other._M) for j in range(other._M))
    return mulParallelEnv(self, other, worker_mul2, indexes_vectors)


def mul3p(self, other):
    """Параллельное умножение матриц с кешированием строки и с использованием скалярного умножения векторов numpy"""
    indexes_vectors = ((j, self._M, self._N, other._M) for j in range(other._M))
    return mulParallelEnv(self, other, worker_mul3, indexes_vectors)


