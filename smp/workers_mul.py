from array import array
import numpy as np

"""Функции организующие паралельное выполнение умножения строк матриц,
 как в потоках, так и на процессах"""

def mulParallelEnv(self, other, function_worker_mul, indexes_vectors):
    """Задает окружение для параллельного выполнения"""
    ret = self.make_result(other)
    pool = ret.getMultiprocessing().Pool(
        processes=self.getComputeUnit(),
        initializer=init_proc,
        initargs=(self._array, other._array, ret._array))
    pool.map(function_worker_mul, indexes_vectors)
    pool.close()
    pool.join()
    return ret

def worker_mul1(arg):
    """Обычное параллельное умножение матриц"""
    # индекс строки, размерность вектора, длина столбца
    index, vector_len, other_M = arg
    for j in range(other_M):  # обход по-столбцам
        sum = 0
        for k in range(vector_len):
            sum += shared_self_array[(index * vector_len) + k] \
                   * shared_other_array[(k * other_M) + j]
        shared_ret_array[(index * other_M) + j] = sum
    return True


def worker_mul2(arg):
    """Параллельное умножение матриц с кешированием строки"""
    # индекс столбца, размерность вектора, длина строки, длина столбца
    index, vector_len, self_N, other_M = arg
    col = array('d', (shared_other_array[(k * other_M) + index] for k in range(vector_len)))

    for i in range(self_N):  # обход по-строкам
        sum = 0
        for k in range(vector_len):
            sum += shared_self_array[(i * vector_len) + k] * col[k]
        shared_ret_array[(i * other_M) + index] = sum
    del col
    return True


def worker_mul3(arg):
    """Параллельное умножение матриц с кешированием строки и с использованием скалярного умножения векторов numpy"""
    # индекс столбца, размерность вектора, длина строки, длина столбца
    index, vector_len, self_N, other_M = arg
    col = array('d', (shared_other_array[(k * other_M) + index] for k in range(vector_len)))
    _col = memoryview(col)
    _shared_self_array = memoryview(shared_self_array)

    for i in range(self_N):  # обход по-строкам
        p = i * vector_len
        sum = np.dot(_shared_self_array[p:p + vector_len], _col[0:vector_len])
        shared_ret_array[(i * other_M) + index] = sum
    del col
    return True



def init_proc(self_array, other_array, ret_array):
    """Функция инициализации массивов для паралельной обработки"""
    #необходимо для связи между процессами
    global shared_self_array, shared_other_array, shared_ret_array
    shared_self_array, shared_other_array, shared_ret_array = self_array, other_array, ret_array
