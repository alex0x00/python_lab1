from array import *
from smp.multiplication_functions import mul1, mul2, mul3, mul4
from struct_matrix import StructMatrix


# TODO добавить умное определени типа массива результата

class Matrix(StructMatrix):
    @classmethod
    def get_mul_methods(cls):
        return [mul1, mul2, mul3, mul4]

    def __init__(self, n, m, initializer=None, type_values='d'):
        StructMatrix._array_type = array
        StructMatrix.__init__(self, n, m, initializer, type_values)

    def __mul__(self, other):
        return Matrix.get_mul_methods()[-1](self, other)
