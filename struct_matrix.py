import operator

#TODO добавить конфигурацию загрузки вычислительных модулей
class StructMatrix:
    _array_type = None
    _use_compute_unit = None
    @classmethod
    def get_mul_methods(cls):
        return []

    @classmethod
    def getComputeUnit(cls):
        return cls._use_compute_unit

    @classmethod
    def setComputeUnit(cls, cu=None):
        cls._use_compute_unit = cu

    def __init__(self, n, m, initializer=None, type_values='d'):
        self._N = n
        self._M = m
        self._type_values = type_values
        if initializer is None:
            self._array = StructMatrix._array_type(type_values, [0 for i in range(n * m)])
        elif callable(initializer):
            self._array = StructMatrix._array_type(type_values, [initializer() for i in range(n * m)])
        else:
            self._array = StructMatrix._array_type(type_values, initializer)

    def __repr__(self):
        return "%s(%d, %d, %s)" % (self.__name__, self._N, self._M, self._array.__repr__)

    def __str__(self):
        def _():
            for i in range(self._N):  # обход по-строчно
                for j in range(self._M):  # обход по-столбцам
                    yield "%6.4f\t" % self.get_item(i, j)
                yield "\n"

        return "".join(_())

    def __bool__(self):
        return self._N > 0 and self._M > 0 and self._array

    def __eq__(self, other):
        return self._N == other._N \
               and self._M == other._M \
               and all(operator.eq(a, b) for a, b in zip(self._array, other._array))

    def __del__(self):
        self._N = 0
        self._M = 0
        del self._array

    def get_item(self, i, j):
        return self._array[(i * self._M) + j]

    def set_item(self, i, j, value):
        self._array[(i * self._M) + j] = value

    def make_result(self, other):
        if self._M != other._N:
            raise NameError('Ошибка: не соразмерные матрицы')
        return self.__class__(self._N, other._M)
